<?php

declare(strict_types=1);

namespace Moave\FreiraumBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MoaveFreiraumBundle extends Bundle
{
}
