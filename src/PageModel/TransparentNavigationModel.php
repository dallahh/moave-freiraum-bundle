<?php

namespace RedpinataBundle\PageModel;

use Contao\PageModel;
use Contao\LayoutModel;
use Contao\PageRegular;

/**
 * Class TransparentNavigationModel
 *
 * Model class for redpinata extended pages
 * Should be used to hold generalized database queries.
 *
 * @package RedpinataBundle\PageModel
 * @author Ronny Dahlke <dahlke@internetinnovations.de>
 */
class TransparentNavigationModel extends PageModel
{
    /**
     * Extends page object with custom data relations
     *
     * @param  PageModel   $objPage        Page object
     * @param  LayoutModel $objLayout      Layout object
     * @param  PageRegular $objPageRegular Regular page object
     *
     * @return void
     */
    public function extendPageObject(
        PageModel &$objPage,
        LayoutModel $objLayout,
        PageRegular $objPageRegular
    ) {
        $objPageRegular->Template->isNaviTransparent = $objPage->arrData['isNaviTransparent'];
    }
}