<?php

/**
 * News List
 *
 * Copyright (c) 2017 moave
 *
 * @package   Freiraum
 * @author    Ronny Dahlke
 * @license   private
 * @copyright moave
 */


/**
 * Namespace
 */
namespace Moave\FreiraumBundle\Module;

use Moave\FreiraumBundle\Model\NewsModel;

/**
 * Class ModuleCastingList
 *
 */
class NewsList extends \Module
{
    /**
     * @var string
     */
    const MODULE_KEY = 'freiraum_news_list';


    /**
     * Template
     * @var string
     */
    protected $strTemplate      = 'mod_freiraum_news_list';



    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {

        if (TL_MODE == 'BE') {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate                = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard      = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['freiraum_news_list'][0]) . ' ###';
            $objTemplate->title         = $this->headline;
            $objTemplate->id            = $this->id;
            $objTemplate->link          = $this->name;
            $objTemplate->href          = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Compile the current element
     */
    protected function compile()
    {
        $newss                = array();
        //$locale                 = \Input::get('language');

        $this->loadLanguageFile(NewsModel::getTableName());

        // Set the item from the auto_item parameter
        if (!isset($_GET['items']) && \Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            \Input::setGet('items', \Input::get('auto_item'));
        }

        // Do not render list items if on detail page
        if (\Input::get('items') != '') {
            return;
        }

        $newsResults     = NewsModel::findTeaserItems();

        // Get jump to page
        if ($this->jumpto !== null && is_int($this->jumpto)) {
            $objTarget = \PageModel::findByPk($this->jumpTo);

            if ($objTarget === null) {
                throw new \Exception('Freiraum Casting List Module needs a valid jump to page.');
            }

            $url = $this->generateFrontendUrl(
                $objTarget->row(),
                \Config::get('useAutoItem') && !\Config::get('disableAlias') ?  '/%s' : '/items/%s'
            );
        }


        foreach ($newsResults as $key => $entry) {
            $entry['preview_image_data'] = \FilesModel::findByUuid($entry['preview_image']);
            $entry['href'] = $this->makeLink($entry['jumpTo']);
            $newss[$key]      = $entry;
            //var_dump($entry['preview_image_data']);
        }

        //var_dump($newss);

        $this->Template->newsList = $newss;
    }

    /**
     * Erstellt den Link zur Detailseite.
     * @param $intId
     * @return string
     */
    private function makeLink($intId)
    {
        $objPage = \Contao\PageModel::findByPk($intId);

        if ($objPage) {
            return $objPage->alias . '.html';
        }

        return '';
    }

}
