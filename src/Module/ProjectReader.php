<?php

/**
 * Project Reader
 *
 * Copyright (c) 2017 moave
 *
 * @package   Freiraum
 * @author    Ronny Dahlke
 * @license   private
 * @copyright moave
 */

/**
 * Namespace
 */
namespace Moave\FreiraumBundle\Module;

use Moave\FreiraumBundle\Model\ProjectModel;

/**
 * Class ModuleProjectReader
 *
 * @copyright  internetinnovations GmbH
 * @author     Claudio De Facci
 * @package    Freiraum
 */
class ProjectReader extends \Module
{

    /**
     * @var string
     */
    const MODULE_KEY = 'freiraum_project_reader';

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_freiraum_project_reader';


    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate                = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard      = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['freiraum_project_reader'][0]) . ' ###';
            $objTemplate->title         = $this->headline;
            $objTemplate->id            = $this->id;
            $objTemplate->link          = $this->name;
            $objTemplate->href          = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['items']) && \Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            \Input::setGet('items', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no entry has been specified
        if (!\Input::get('items')) {
            /** @var \PageModel $objPage */
            global $objPage;

            $objPage->noSearch = 1;
            $objPage->cache = 0;
        }

        return parent::generate();
    }


    /**
     * Compile the current element
     */
    protected function compile()
    {

        // Set the item from the auto_item parameter
        if (!isset($_GET['items']) && \Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            \Input::setGet('items', \Input::get('auto_item'));
        }

        $locale                 = \Input::get('language');

        $this->loadLanguageFile(ProjectModel::getTableName());

        $projectItem             = ProjectModel::findOneByAlias(\Input::get('items'));


        // Get jump to page
        if (isset($this->jumpTo)) {
            $objTarget = \PageModel::findByPk($this->jumpTo);

            if ($objTarget !== null) {
                $this->Template->projectOverview   = $this->generateFrontendUrl($objTarget->row());
            } else {
                throw new \Exception('Freiraum Project Navigation Module needs a valid jump to page (to project overview page).');
            }
        }


        $this->Template->projectItem = $projectItem;
        //$this->Template->alternativeLanguages = Language::findOtherLanguageAliases(ProjectModel::getTableName(), $projectItem);
    }
}
