<?php

namespace Moave\FreiraumBundle\Elements;

/**
 * Content stage video.
 *
 * @author Ronny Dahlke
 */
class ContentTwoColText extends \ContentElement
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_two_col_text';


    /**
     * Initialize the object
     *
     * @param \ContentModel $objElement
     * @param string        $strColumn
     */
    public function __construct($objElement, $strColumn = 'main')
    {
        parent::__construct($objElement, $strColumn);


        }

    /**
     * Generate the content element
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            return '<h3>'.$GLOBALS['TL_LANG']['CTE']['ce_two_col_text'][0].'</h3><p>'.$GLOBALS['TL_LANG']['CTE']['ce_two_col_text'][1].'</p>';
        }

        return parent::generate();
    }


    /**
     * Generate the content element
     */
    protected function compile()
    {

        return;
    }
}
