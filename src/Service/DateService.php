<?php

namespace Moave\FreiraumBundle\Services;

class DateService
{
	/**
	 * List of date formats per locale
	 * @var	array
	 */
	protected static $dateFormats = [
		'default'		=> 'd.m.Y',

		'de_DE'			=> 'd.m.Y',
		'en_US'			=> 'm/d/Y',
		'zh_Hans_CN'	=> 'Y.m.d',
	];


	/**
	 * Format date for a certain locale
	 * @param	integer|timestamp	$date 		Date timestamp
	 * @param	string				$locale		Locale to format for, i.e. de_DE
	 * @return	string							Formated date
	 */
	public static function formatByLocale($date, $locale)
	{
		$format			= self::$dateFormats['default'];
		if (array_key_exists($locale, self::$dateFormats)) {
			$format		= self::$dateFormats[$locale];
		}

		return date($format, $date);
	}
}
