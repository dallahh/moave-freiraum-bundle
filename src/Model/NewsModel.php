<?php

namespace Moave\FreiraumBundle\Model;

//use FreiraumBundle\Service\DateService;

/**
 * Model class for Genesis module
 *
 * Gets loaded magically.
 * Should be used to hold generalized database queries.
 */
class NewsModel extends \Contao\Model
{
    /**
     * @var string
     */
    protected static $strTable = 'tl_freiraum_news';


    /**
     * @return string
     */
    public static function getTableName()
    {
        return static::$strTable;
    }

    /**
     * @param $newsId
     *
     * @return array
     */
    public function getAssocNewsById($newsId)
    {
        $db 	  = \Database::getInstance();
        $sqlQuery = 'SELECT * FROM ' . static::$strTable . ' WHERE id=?';

        $result = $db
            ->prepare($sqlQuery)
            ->execute($newsId);

        return $result->fetchAllAssoc();
    }

    // Used for detail page
    public static function findOneByAlias($alias)
    {
        $db         = \Database::getInstance();
        $result     = $db->prepare("SELECT c.id, c.title
                            FROM ".self::$strTable." as c
                            WHERE
                                c.alias=?
                                
                            LIMIT 1")
            ->execute($alias);

        return $result->fetchAssoc();
    }


    /**
     * @param $locale
     * @param int $limit
     * @return array
     */
    public static function findTeaserItems($limit = 40)
    {
        $db         = \Database::getInstance();
        $sqlQuery = 'SELECT id, title, description, preview_image, jumpTo FROM ' . static::$strTable . ' ORDER BY sorting ASC  LIMIT ?';

        $result = $db
            ->prepare($sqlQuery)
            ->execute($limit);

        return $result->fetchAllAssoc();
    }
}
