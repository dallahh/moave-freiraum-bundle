<?php

namespace Moave\FreiraumBundle\Model;

//use FreiraumBundle\Service\DateService;

/**
 * Model class for Genesis module
 *
 * Gets loaded magically.
 * Should be used to hold generalized database queries.
 */
class ProjectModel extends \Contao\Model
{
    /**
     * @var string
     */
    protected static $strTable = 'tl_freiraum_project';


    /**
     * @return string
     */
    public static function getTableName()
    {
        return static::$strTable;
    }

    /**
     * @param $projectId
     *
     * @return array
     */
    public function getAssocProjectById($projectId)
    {
        $db 	  = \Database::getInstance();
        $sqlQuery = 'SELECT * FROM ' . static::$strTable . ' WHERE id=?';

        $result = $db
            ->prepare($sqlQuery)
            ->execute($projectId);

        return $result->fetchAllAssoc();
    }

    // Used for detail page
    public static function findOneByAlias($alias)
    {
        $db         = \Database::getInstance();
        $result     = $db->prepare("SELECT c.id, c.title
                            FROM ".self::$strTable." as c
                            WHERE
                                c.alias=?
                                
                            LIMIT 1")
            ->execute($alias);

        return $result->fetchAssoc();
    }


    /**
     * @param $locale
     * @param int $limit
     * @return array
     */
    public static function findTeaserItems($limit = 80)
    {
        $db         = \Database::getInstance();
        $sqlQuery = 'SELECT id, title, description, preview_image, jumpTo, published, ordernumber FROM ' . static::$strTable . ' WHERE published = "1" ORDER BY sorting ASC LIMIT ?';

        $result = $db
            ->prepare($sqlQuery)
            ->execute($limit);

        return $result->fetchAllAssoc();
    }
}
