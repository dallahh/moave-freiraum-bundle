<?php

$strName = 'tl_content';


/* Palettes */
$GLOBALS['TL_DCA'][$strName]['palettes']['ce_two_col_text'] = '{type_legend},type,headline;{description_legend},lefttext,righttext;{invisible_legend:hide},invisible,start,stop;';


/* Fields */
$GLOBALS['TL_DCA'][$strName]['fields']['lefttext'] = array
(
    'label'                   => &$GLOBALS['TL_LANG'][$strName]['lefttext'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte' => 'tinyMCE'),
    'sql'                     => "text"
);

$GLOBALS['TL_DCA'][$strName]['fields']['righttext'] = array
(
    'label'                   => &$GLOBALS['TL_LANG'][$strName]['righttext'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte' => 'tinyMCE'),
    'sql'                     => "text"
);
