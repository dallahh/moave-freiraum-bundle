<?php

use Moave\FreiraumBundle\Model\NewsModel;

#use FreiraumBundle\MediaModel;

#use FreiraumBundle\ImagePreset;
#use FreiraumBundle\PublishPreset;


$moduleTable = NewsModel::getTableName();

/**
 * Load tl_content language file
 */
\System::loadLanguageFile($moduleTable);
\System::loadLanguageFile('freiraum_image');


/**
 * Table freiraum_news
 */
$GLOBALS['TL_DCA'][$moduleTable] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'onsubmit_callback' => [
            [$moduleTable, 'createVersion'],
        ],
        'ondelete_callback' => [
            [$moduleTable, 'createVersion'],
        ],
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2, //Tree-View
            'fields'                  => array('sorting')
        ),

        'label' => array
        (
            'fields' => array('backendtitle'),
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['freiraum_gui']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ),
            'copy' => array
            (
                'label' => &$GLOBALS['TL_LANG']['freiraum_gui']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['freiraum_gui']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['freiraum_gui']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array(),
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array(),
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{title_legend},backendtitle,title,description,preview_image,images;{redirect_legend},jumpTo,published,sorting',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment",
        ),

        'pid' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),

        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ),

        'backendtitle' => array
        (
            'label' => &$GLOBALS['TL_LANG'][$moduleTable]['backendtitle'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr long'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),

        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG'][$moduleTable]['title'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'clr'),
            'sql' => "text NULL",
        ),

        'description' => array
        (
            'label' => &$GLOBALS['TL_LANG'][$moduleTable]['description'],
            'exclude' => true,
            'inputType' => 'textarea',
            'search' => true,
            'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'clr'),
            'sql' => "text NULL",
        ),

        'preview_image' => array
        (
            'label' => &$GLOBALS['TL_LANG'][$moduleTable]['previewImage'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
            ),
            'sql' => "binary(16) NULL"
        ),

        'published' => array
        (
            'exclude' => true,
            'label' => &$GLOBALS['TL_LANG']['freiraum_gui']['published'],
            'inputType' => 'checkbox',
            'eval' => array('submitOnChange' => true, 'doNotCopy' => true, 'tl_class' => 'clr'),
            'sql' => "char(1) NOT NULL default ''",
        ),

        'jumpTo' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_content']['jumpTo'],
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => array('fieldType'=>'radio'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'hasOne', 'load'=>'eager')
        ),

        'sorting' => array
        (
            'label' => &$GLOBALS['TL_LANG'][$moduleTable]['sorting'],
            'inputType' => 'text',
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),


    )
);


// Add preset subpalettes
//$GLOBALS['TL_DCA'][$moduleTable]['subpalettes']                 = array_merge( $GLOBALS['TL_DCA'][$moduleTable]['subpalettes'], ImagePreset::getSubPalettes('', true) );
//$GLOBALS['TL_DCA'][$moduleTable]['subpalettes']                 = array_merge( $GLOBALS['TL_DCA'][$moduleTable]['subpalettes'], PublishPreset::getSubPalettes() );
//$GLOBALS['TL_DCA'][$moduleTable]['palettes']['__selector__'][]  = PublishPreset::getSelectorPalettes();

// Add lit operations
//$GLOBALS['TL_DCA'][$moduleTable]['list']['operations']          = array_merge($GLOBALS['TL_DCA'][$moduleTable]['list']['operations'], PublishPreset::getToggle($moduleTable));

// Add preset fields
#$GLOBALS['TL_DCA'][$moduleTable]['fields']                      = array_merge(
# $GLOBALS['TL_DCA'][$moduleTable]['fields'], ImagePreset::getFields($moduleTable, '', true) );
//$GLOBALS['TL_DCA'][$moduleTable]['fields']                      = array_merge( $GLOBALS['TL_DCA'][$moduleTable]['fields'], PublishPreset::getFields() );


// Remove unused preset fields
#$GLOBALS['TL_DCA'][$moduleTable]['palettes']['default']         = str_replace(',geoRadius', '',
# $GLOBALS['TL_DCA'][$moduleTable]['palettes']['default']);


// Field additions
//$GLOBALS['TL_DCA'][$moduleTable]['fields']['addressCity']['eval']['mandatory'] = true;


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @author Claudio De Facci <defacci@internetinnovations.de>
 */
class tl_freiraum_news extends Backend
{

    public static $moduleTable;

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Auto-generate the news alias if it has not been set yet
     *
     * @param mixed $varValue
     * @param DataContainer $dc
     *
     * @return string
     *
     * @throws Exception
     */
    public function generateAlias($varValue, \DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if ($varValue == '') {
            $autoAlias = true;
            $varValue = StringUtil::generateAlias($dc->activeRecord->title);
        }

        $objAlias = $this->Database->prepare("SELECT id FROM " . NewsModel::getTableName() . " WHERE alias=?")
            ->execute($varValue);

        // Check whether the alias exists
        if ($objAlias->numRows > 1 && !$autoAlias) {
            throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
        }

        // Add ID to alias
        if ($objAlias->numRows && $autoAlias) {
            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Return the link picker wizard
     *
     * @param DataContainer $dc
     *
     * @return string
     */
    public function pagePicker(\DataContainer $dc)
    {
        return ImagePreset::pagePicker($dc);
    }

    /**
     * Pre-fill the "alt" and "caption" fields with the file meta data
     *
     * @param mixed $varValue
     * @param DataContainer $dc
     *
     * @return mixed
     */
    public function storeFileMetaInformation($varValue, DataContainer $dc)
    {
        return ImagePreset::storeFileMetaInformation($varValue, $dc);
    }

    /**
     * Language specific callbacks
     */
    // public function getLanguageParentEntries(\DataContainer $dc)
    // {
    //     return LanguagePreset::getLanguageParentEntries($dc, self::$moduleTable, 'title');
    // }

    public function getRelationlessLocales(\DataContainer $dc)
    {
        return LanguagePreset::getRelationlessLocales($dc, self::$moduleTable);
    }

    /**
     * Publish: Return the "toggle visibility" button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return PublishPreset::toggleIcon(self::$moduleTable, $this, $row, $href, $label, $title, $icon, $attributes);
    }

    /**
     * Publish: Change published state
     *
     * @param integer $intId
     * @param boolean $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, \DataContainer $dc = null)
    {
        return PublishPreset::toggleVisibility(self::$moduleTable, $intId, $blnVisible);
    }


    public function createVersion(\DataContainer $dc)
    {
        $objVersions = new Versions(self::$moduleTable, $dc->activeRecord->id);
        $objVersions->initialize();
        $objVersions->create();
    }


    /**
     * Dynamically add flags to the "multiSRC" field
     *
     * @param mixed $varValue
     * @param DataContainer $dc
     *
     * @return mixed
     */
    public function setMultiSrcFlags($varValue, DataContainer $dc)
    {
        if ($dc->activeRecord) {
            switch ($dc->activeRecord->type) {
                case 'gallery':
                    $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['isGallery'] = true;
                    $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['extensions'] = Config::get('validImageTypes');
                    break;

                case 'downloads':
                    $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['isDownloads'] = true;
                    $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['eval']['extensions'] = Config::get('allowedDownload');
                    break;
            }
        }

        return $varValue;
    }

}

tl_freiraum_news::$moduleTable = $moduleTable;


class tl_ms_test extends Backend
{

    public function pasteElement(DataContainer $dc, $row, $table)
    {
        var_dump("sdfdsfs");
        die();
        $imagePasteAfter = Image::getHtml('pasteafter.gif', sprintf($GLOBALS['TL_LANG'][$table]['pasteafter'][1], $row['id']));
        return '<a href="'.$this->addToUrl('act=cut&mode=1&pid='.$row['id']).'" title="'.specialchars(sprintf($GLOBALS['TL_LANG'][$table]['pasteafter'][1], $row['id'])).'" onclick="Backend.getScrollOffset()">'.$imagePasteAfter.'</a> ';

    }

}