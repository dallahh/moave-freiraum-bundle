<?php

use Moave\FreiraumBundle\Model\ProjectModel;
use Moave\FreiraumBundle\Model\NewsModel;
use Moave\FreiraumBundle\Module\ProjectList;
use Moave\FreiraumBundle\Module\NewsList;
use Moave\FreiraumBundle\Module\ProjectReader;

/**
 * DEACTIVATED BACKEND MODULES & CONTENT ELEMENTS
 * Remove if not needed
 */

/**
unset($GLOBALS['BE_MOD']['content']['news']);
unset($GLOBALS['BE_MOD']['content']['calendar']);
unset($GLOBALS['BE_MOD']['content']['faq']);
unset($GLOBALS['BE_MOD']['content']['newsletter']);
unset($GLOBALS['BE_MOD']['content']['comments']);

unset($GLOBALS['TL_CTE']['texts']['text']);
unset($GLOBALS['TL_CTE']['texts']['list']);
unset($GLOBALS['TL_CTE']['texts']['table']);
unset($GLOBALS['TL_CTE']['texts']['code']);
unset($GLOBALS['TL_CTE']['texts']['markdown']);

unset($GLOBALS['TL_CTE']['media']['image']);
unset($GLOBALS['TL_CTE']['media']['gallery']);
unset($GLOBALS['TL_CTE']['media']['player']);
unset($GLOBALS['TL_CTE']['media']['youtube']);

// unset($GLOBALS['TL_CTE']['accordion']);
unset($GLOBALS['TL_CTE']['slider']);
unset($GLOBALS['TL_CTE']['links']);
unset($GLOBALS['TL_CTE']['files']);

unset($GLOBALS['TL_CTE']['includes']['form']);
 */

/**
 * Add additional languages
 */
//$GLOBALS['TL_LANG']['LNG']['zh_Hans_CN']                            = 'Chinese (Simple)';


/**
 * Additional wrapper CEs
 */
//$GLOBALS['TL_WRAPPERS']['start'][]  = 'freiraum_stage_slider_start';
//$GLOBALS['TL_WRAPPERS']['stop'][]   = 'freiraum_stage_slider_stop';


/**
 * MODELS
 * Required for specific model registration.
 * Used for DCA classes that extend \Model.
 */
//$GLOBALS['TL_MODELS']['tl_freiraum_event']                            = 'Freiraum\EventModel';
$GLOBALS['TL_MODELS']['tl_freiraum_project']                          = 'Moave\FreiraumBundle\Model\ProjectModel';
$GLOBALS['TL_MODELS']['tl_freiraum_news']                             = 'Moave\FreiraumBundle\Model\NewsModel';


/**
 * BACKEND MODULES
 */
array_insert($GLOBALS['BE_MOD']['freiraum'], 1, array
(
    'Project' => array
    (
        'tables'      => [ ProjectModel::getTableName() ],
        'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    ),

    'Aktuelles' => array
    (
        'tables'      => [ NewsModel::getTableName() ],
        'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    ),
//    'Event' => array
//    (
//        'tables'      => array( EventModel::getTableName() ),
//        'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
//    ),
//
//    'Casting' => array
//    (
//        'tables'        => array( CastingModel::getTableName() ),
//        'icon'          => 'system/modules/freiraum/assets/img/icon.gif',
//    ),

    // Deactivated
    // 'FAQ' => array
    // (
    //     'tables'      => array( FAQModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),
    // 'FAQ Category' => array
    // (
    //     'tables'      => array( FAQCategoryModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),
    // 'FAQ Statistic' => array
    // (
    //     'tables'      => array( FAQStatisticModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),

    // Deactivated
    // 'Media' => array
    // (
    //     'tables'      => array( MediaModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),
    // 'Media Category' => array
    // (
    //     'tables'      => array( MediaCategoryModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),
    // 'Media Statistic' => array
    // (
    //     'tables'      => array( MediaStatisticModel::getTableName() ),
    //     'icon'        => 'system/modules/freiraum/assets/img/icon.gif',
    // ),
));


/**
 * FRONTEND MODULES
 */
array_insert($GLOBALS['FE_MOD'], 1, array
(
    'freiraum' => array
    (
        ProjectList::MODULE_KEY                             => ProjectList::class,
        ProjectReader::MODULE_KEY                             => ProjectReader::class,
        NewsList::MODULE_KEY                             => NewsList::class,
//        'freiraum_casting_filter'                         => 'Freiraum\ModuleCastingFilter',
//        'freiraum_casting_list'                           => 'Freiraum\ModuleCastingList',
//        'freiraum_casting_reader'                         => 'Freiraum\ModuleCastingReader',
    )
));


/**
 * Content elements
 */
array_insert($GLOBALS['TL_CTE'], 1, array
(
    'freiraum' => array
    (
        'ce_two_col_text'                            => 'Moave\FreiraumBundle\Elements\ContentTwoColText',
    ),
));


/**
 * Back end form fields
 */
//$GLOBALS['BE_FFL']['headlineAdvanced']                  = 'Freiraum\HeadlineAdvanced';
//$GLOBALS['BE_FFL']['information']                       = 'Freiraum\Widget\Information';


/**
 * HOOKS
 */
//array_unshift($GLOBALS['TL_HOOKS']['generateFrontendUrl'], array('Freiraum\Language', 'generateFrontendUrl'));
//
//$GLOBALS['TL_HOOKS']['replaceInsertTags'][]             = array('Freiraum\InsertTags', 'getTranslation');
//
#$GLOBALS['TL_HOOKS']['generatePage'][]  = [
//    'FreiraumBundle\PageModel\TransparentNavigationModel',
//    'extendPageObject'
#];
//$GLOBALS['TL_HOOKS']['generatePage'][]                  = array('\PageModel', 'addCustomFields');
//
//$GLOBALS['TL_HOOKS']['getImage'][]                      = array('Freiraum\ImageCache', 'createMissingCacheDirectories');
//
//$GLOBALS['TL_HOOKS']['getSearchablePages'][]            = array('Freiraum\Sitemap', 'fixWrongLocales');
//
//$GLOBALS['TL_HOOKS']['isVisibleElement'][]              = array('Freiraum\ContentElementFixes', 'isVisibleElement');
//
//$GLOBALS['TL_HOOKS']['generatePage'][]                  = array('Freiraum\MetaTags', 'fixLanguageAlternatives');
//
//$GLOBALS['TL_HOOKS']['loadLanguageFile'][]              = array('Freiraum\Translations', 'addCustomTranslations');
//
//$GLOBALS['TL_HOOKS']['generatePage'][]                  = array('Freiraum\CacheBuster', 'getBuildNumber');
//
//// CN locale URL fix
//$GLOBALS['TL_HOOKS']['i18nl10n_generateFrontendUrl'][]  = array('Freiraum\ChineseLocaleFix', 'generateFrontendUrl');
//$GLOBALS['TL_HOOKS']['i18nl10n_getPageIdFromUrl'][]     = array('Freiraum\ChineseLocaleFix', 'getPageIdFromUrl');
//$GLOBALS['TL_HOOKS']['i18nl10n_getSearchablePages'][]   = array('Freiraum\Sitemap', 'formatUrlsByRegion');


/**
 * Languages
 */
//$GLOBALS['YUNEEC_LANGUAGES'] = array(
//
//    'short' => array(
//        'en'            => 'EN',
//        'en_GB'         => 'EN',
//        'en_US'         => 'EN',
//        'de_DE'         => 'DE',
//        'zh_Hans_CN'    => 'ZH',
//
//        'it_IT'         => 'IT',
//        'es_ES'         => 'ES',
//        'fr_FR'         => 'FR',
//        'nl_NL'         => 'NL',
//        'fi_FI'         => 'FI',
//
//        'zh_TW'         => 'ZH',
//        'ja_JP'         => 'JA',
//        'ko_KR'         => 'KO',
//
//        'de_AT'         => 'DE',
//        'de_CH'         => 'CH',
//        'fr_CH'         => 'CH',
//        'nl_BE'         => 'BE',
//        'fr_BE'         => 'BE',
//        'tr_TR'         => 'TR',
//        'pt_PT'         => 'PT',
//        'cs_CZ'         => 'CZ',
//        'ru_RU'         => 'RU',
//        'pl_PL'         => 'PL',
//
//    ),
//
//    'long' => array(
//        'en'            => 'International',
//        'en_GB'         => 'United Kingdom',
//        'en_US'         => 'United States',
//        'de_DE'         => 'Deutschland',
//        'zh_Hans_CN'    => '中华人民共和国',
//
//        'it_IT'         => 'Italia',
//        'es_ES'         => 'España',
//        'fr_FR'         => 'France',
//        'nl_NL'         => 'Nederland',
//        'fi_FI'         => 'Suomi',
//
//        'zh_TW'         => '台湾',
//        'ja_JP'         => '日本',
//        'ko_KR'         => '한국',
//
//        'de_AT'         => 'Österreich',
//        'de_CH'         => 'Schweiz (DE)',
//        'fr_CH'         => 'Suisse (FR)',
//        'nl_BE'         => 'België (NL)',
//        'tr_TR'         => 'Türkiye',
//        'pt_PT'         => 'Portugal',
//        'cs_CZ'         => 'Česko',
//        'ru_RU'         => 'Россия',
//        'pl_PL'         => 'Polska',
//
//    ),
//);
