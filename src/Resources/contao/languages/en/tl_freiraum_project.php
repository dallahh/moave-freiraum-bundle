<?php

use Moave\FreiraumBundle\Model\ProjectModel;

$moduleTable = ProjectModel::getTableName();

$GLOBALS['TL_LANG']['MOD'][$moduleTable] = 'Projekt Einträge';

$GLOBALS['TL_LANG'][$moduleTable] = [
	/**
	 * Legends
	 */
	'title_legend' 		=> 'Projekt Titel',

	/**
	 * Fields
	 */
	'title'				=> array('Title', 				'Please provide a title.'),
	'introText'			=> array('Introduction', 		'Please provide an intro text.'),
	'text'				=> array('Text', 				'Please provide a text.'),
	'location'			=> array('Location', 			'Please provide a location.'),
	'country'			=> array('Country', 			'Please provide a country.'),
	'contactEmail'		=> array('Contact  Email', 		'Please provide a contact email address.'),
	'category'			=> array('Category', 			'Please provide a category.'),

	/**
	 * Gui
	 */
	'new'    			=> array('New ', 				'Create a new project entry.'),
];
