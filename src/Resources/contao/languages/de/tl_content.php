<?php

// Basics
$GLOBALS['TL_LANG']['tl_content']['featured'] 					= array('Featured', 'Mark this entry as special/highlighted.');

// Stage
$GLOBALS['TL_LANG']['tl_content']['stageSize'] 					= array('Stage size', 'Big [ Aspect Ratio: 16:9 / max. Dimension: 2800x1344px ]...  Normal [ Aspect Ratio: 7:2 / max. Dimension: 2800x852px ]');