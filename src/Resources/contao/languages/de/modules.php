<?php

use Moave\FreiraumBundle\Module\ProjectList;


$GLOBALS['TL_LANG']['MOD']['freiraum'][0] 					= 'Freiraum';
$GLOBALS['TL_LANG']['MOD']['freiraum'][1] 					= 'Freiraum verwalten';


/**
 * Frontend modules
 */
$GLOBALS['TL_LANG']['FMD']['freiraum'] 						= 'Freiraum';


// Career
$GLOBALS['TL_LANG']['FMD']['freiraum_project_filter'] = array
(
    'Casting Filter',
    'Lists of available casting filters.',
);

$GLOBALS['TL_LANG']['FMD'][ProjectList::MODULE_KEY] = array
(
    'Projekt Liste',
    'Lists all available casting entries.',
);

$GLOBALS['TL_LANG']['FMD']['freiraum_project_reader'] = array
(
    'Casting Reader',
    'Shows a specific casting entry.',
);

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['freiraum'] 						= 'Freiraum';
