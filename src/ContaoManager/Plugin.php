<?php

declare(strict_types=1);

namespace Moave\FreiraumBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Moave\FreiraumBundle\MoaveFreiraumBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(MoaveFreiraumBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
                ->setReplace(['moave-freiraum']),
        ];
    }
}
